
from hashlib import sha1

""" This program reads one of Pascal's tXXX files and splits it into one
    file per composite.
    
    TODO: Update this to remove old files when a factor has been found for
    a given branch"""

def main():
    
    with open("t2200.txt") as f:
        for line in f:
            p, q, c = line.strip().split(' ')
            
            #I'm assuming that all composites with less than 130 digits will be factored by someone else
            if len(c) < 130:
                continue

            #Use the prime and power to identify the composite
            branch_id = sha1(' '.join([p, q])).hexdigest()[:8]
            
            filename = "C{:04d}_{}.composite".format(len(c), branch_id)
            
            with open(filename, 'w') as outfile:
                outfile.write(c)
                outfile.write('\n')


if __name__ == "__main__":
    main()
